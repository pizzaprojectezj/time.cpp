#include <stdio.h>
#include <iostream>
#include "time.h"

using namespace std;


/* Constructor
Post: Creates a time with default values for data members */
Time::Time()
{
    
    min = 0;
    hour = 0;
    
} 


/* Copy constructor
 *
 * Pre: none
 * Post: Initializes the driver to be equivalent to the other driver.
 * */
Time::Time(const Time& other)
{
    operator = (other);
}

/* Overloaded assignment operator
 *
 * Pre: none
 * Post: Overloaded assignment operator. Sets the  Driver to be equivalent to the source
 * object parameter and returns a reference to the modified driver.
 * */
Time& Time::operator = (const Time& other)
{
    if(this != &other) {
        this -> hour = other.hour;
        this -> min = other.min;
    }
    return *this;
}


/* Time
Pre: 0 <= hour <= 23 and 0 <= min <= 59
Post: Creates a time with the given hour and minute */
Time::Time(int hour, int min) throw (logic_error)
{
    if(hour > 23 || min > 59)
        throw logic_error("Incorrect time, hours must be < 24 and  minutes < 60");


}


/* elapsedMin
Post: Returns the difference between t1 and t2. Assumes t2 is between 00:00 and 23:59 hours after t1 */
int Time::elapsedMin(Time t1, Time t2)
{
    int TotalT1 = ((t1.hour * 60)+ t1.min);
    int TotalT2 = ((t2.hour * 60)+ t2.min);
    
    if(TotalT2 < TotalT1)
    {
        TotalT2 = TotalT2 + 1440;
    }
    
    return (TotalT2 - TotalT1);

}


/* toString
Post: Returns a string containing the hour and minute (e.g., "13:01) */
string Time::toString()
{
    return to_string(hour) + ":" + to_string(min);

}
