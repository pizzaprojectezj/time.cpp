#ifndef TIME_H
#define TIME_H

#include <iostream>
#include <ctime>
#include <string>
#include <stdexcept>
using namespace std;

class Time
{
    
public:
    
    /* Constructor
     Post: Creates a time with default values for data members */
    Time();
    
    /* Time
     Pre: 0 <= Ahour <= 23 and 0 <= min <= 59
     Post: Creates a time with the given hour and minute */
    Time(int Ahour, int Amin) throw (logic_error);
    
    /* elapsedMin
     Post: Returns the difference between t1 and t2. Assumes t2 is between 00:00 and 23:59 hours after t1 */
    static int elapsedMin(Time t1, Time t2);
    
    /* toString
     Post: Returns a string containing the hour and minute (e.g., "13:01) */
    string toString();
    
private:
    
    /* Current private members, may change while writing functions */
    int hour;
    int min;
    
};

#endif
